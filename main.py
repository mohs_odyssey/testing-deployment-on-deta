from fastapi import FastAPI
from deta import Deta
from pydantic import BaseModel


app = FastAPI()

deta = Deta()

users = deta.Base("users")

class User(BaseModel):
    name: str
    age: int
    email: str
    password: str


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/users")
def read_users():
    return users.fetch()

@app.post("/users")
def create_user(user: User):
    return users.put(user.dict())


@app.get("/users/{user_id}")
def read_user(user_id:str):
    return users.get(user_id)


@app.put("/users/{user_id}")
def update_user(user_id:str, update:dict):
    return users.update(update, user_id)


@app.delete("/users/{user_id}")
def delete_user(user_id:str):
    return users.delete(user_id)